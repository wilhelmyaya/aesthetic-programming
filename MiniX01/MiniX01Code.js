function setup() {
  createCanvas(594, 841);
  background(230, 221, 200);
  // describe('a canvas with a beige color');
}

function draw() {
  const c1 = color(231, 200, 73); 
  fill(c1); //yellow
  noStroke();
  rect(20, 20, 380, 800);
  // describe('big yellow rectangle');
  
  const c2 = color(45, 103, 151); 
  fill(c2); //blue
  circle(400, 225, 270);
  // describe('a blue circle');
  
  const c4 = color(3,6,8);
  fill(c4); //black
  rect(20,700,554,25);
  // describe('a black stripe');
  
  const c5 = color(201,59,47);
  fill(c5); //red
  rect(80, 20, 25, 800);
  //describe('a red stripe');
  
  fill(c1); //yellow
  rect(105,725,469,95);
  //describe('yellow bottom');
  
  fill(c2); //blue
  rect(20,725,60,95)
  //describe('bottom right blue rectangle');
  
  fill(c2); //blue
  rect(105,605,95,95);
  //describe('blue square');
  
  fill(c5); //red
  rect(200,500,200,200);
  
}
