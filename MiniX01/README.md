# MiniX01: Bauhaus 01 Reproduction

![p5.js recreation of generic Bauhaus poster](https://scontent-arn2-1.xx.fbcdn.net/v/t1.15752-9/429841507_783307907000404_4902747527204848149_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=8cd0a2&_nc_aid=0&_nc_ohc=JRUqh6wWoasAX9ezZhJ&_nc_ht=scontent-arn2-1.xx&oh=03_AdRBe9DWZ7ufsygdKcSQeiaaWq4eoaggx0lc0kLjYbUDWw&oe=66090886)
<br>

Please run the code [here](https://editor.p5js.org/willemussemand/full/woTjAD03w)
<!--- Linket skal formatteres sÃ¥dan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->
<br>
Please view the full repository [here](https://gitlab.com/WilhelmYAYA/aesthetic-programming/-/tree/main/miniX01)
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked nÃ¥r I befinder jer I mappen -->

## The Project

This was my very first MiniX exercise, and the first project that I made using p5. This was a very fun exercise, wherein I focused on recreating a generic Bauhaus poster, which i link in the refrences. This was in order to get a grip on the purely aesthetic aspects of p5.

### the Colors
My process for this piece involved downloading the refrence image, putting it into procreate, and using the color picker tool in order to get the precise RGB values for every color. In this way, I was able to emulate the color of the original piece as perfectly as possible.

### the Shapes
As for the shapes in the piece, the sizes and dimensions were mostly eyeballed. This was simply because i lacked the right tools for measuring the refrence piece. 

### the Code 
In terms of the code I used for the project, it was quite simple. Simple shapes, with simple RGB values. There was no real interaction, so in this way, it was quite simple to make.

## **References**
<!-- Eksempler pÃ¥ referencer her, pretty much bare links med line breaks mellem dem-->
[Refrences from p5js.org](https://p5js.org/reference/#/p5/frameRate)
<br>
[Refrence image from lefthandprints.co.uk](https://lefthandprints.co.uk/shop/p/angel-of-the-north-art-print-poster-dc5nj-le8b3-ngbc2-r9msh)
