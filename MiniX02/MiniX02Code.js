let diameter = 600;
let angle = 0;
let rSlider, gSlider, bSlider;

function setup() {
  createCanvas(800, 800);
  background('pink');

  // sliders for RGB control
  rSlider = createSlider(0, 255, 255); // Range from 0 to 255, starting at (255, 255, 0) creating a yellow smiley
  rSlider.position(20, 20); // Position of sliders
  gSlider = createSlider(0, 255, 255);
  gSlider.position(20, 50);
  bSlider = createSlider(0, 255, 0);
  bSlider.position(20, 80);
}

function draw() {
  // Collects data from the sliders in order to determine RGB value
  let r = rSlider.value();
  let g = gSlider.value();
  let b = bSlider.value();

  strokeWeight(20);
  fill(r, g, b); // Use RGB values from sliders
  
  frameRate(30); //determines smoothness of pulsing
  angle += 0.05; //determines speed 
  let pulsate = sin(angle) * 20; //creates 
  let newDiameter = diameter + pulsate;
  
  if (newDiameter < 0) {
    newDiameter = 0;
  }
  
  circle(400, 400, newDiameter);

  const c1 = color('white');
  fill(c1);
  strokeWeight(10);
  ellipse(550, 300, 60, 120); //eyes
  ellipse(250, 300, 60, 120);
  
  fill('red')
  circle(550, 320, 40, 20); //pupils
  circle(250, 320, 40, 20);

  strokeWeight(20);
  noFill();
  arc(400, 500, 400, 100, 0, PI); //smile
  arc(400, 650, 150, 75, 0, PI); //chin
