# MiniX02: Smiley for Everyone

![p5.js pulsating kind of scary looking smiley with customizable RGB values](https://scontent-cph2-1.xx.fbcdn.net/v/t1.15752-9/429889612_2789589601195227_6075912275990949158_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=5f2048&_nc_ohc=ZYOmIjCFpEQAX_DjdYT&_nc_ht=scontent-cph2-1.xx&oh=03_AdRyNRzhwruRE2uxjruE_r3uF9xy7BCZFJPM61IfG_I6RQ&oe=6613E9B6)
<br>

Please run the code [here](https://editor.p5js.org/willemussemand/full/fAyP5f2zf)
<br>
Please view the full repository [here](https://gitlab.com/WilhelmYAYA/aesthetic-programming/-/tree/main/miniX02)
<br>

## THE PROJECT: IDEAS

Emojis are becoming more and more inclusive of different races, genders, and sexualities. holding down on an emoji, will give you 6 different options for different skin colors, and tones. I wanted to take this to the very extreme, with the user being able to choose any combination of red, green, and blue in order to represent every possible skin color. 

the classical yellow color of the emojis we all use in our daily lives has become imprinted in our heads as a standard, and I wanted to see what it would look like, with fx. a purple smiley, or maybe a green one. 

I also made the smiley pulsate, in order to give some sort of illusion of being alive. 

## THE PROJECT: TECHNIQUES

In the beginning i made some simple shapes in order to represent a smiley face. 

#### pulsation
I introducted the variable, angle, which i used in order to calculate the sine value which will be used for the pulsating effect. I used the sin() function in order to caluculate the sine value of the angle. I then take this value, and multiply it by 20 in order to determine the amplitude of the effect.

The code then takes this amplitude and adds it to the diameter of the circle. The new cirle is then drawn. The incriment of the angle, 0.05, and the amount you multiply the sine value by, in this case 20, controls the intensity and speed of the effect.

#### color
I added 3 sliders to the code, respectively controlling R, G, and B. these sliders range from a value of 0 to 255, allowing for 8 bit colors. I added a 3 functions that take the value from the sliders, and then combine them into a single RGB value. This value is then put on as the skintone of the smiley.

I put the standard slider values as (255, 255, 0), so that the smiley starts of being the classical yellow color.


### REFRENCES

[Refrences from p5js.org](https://p5js.org/reference/#/p5/frameRate)


