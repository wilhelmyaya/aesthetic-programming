function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);   
}

function draw() {
  background(0, 90);
  drawElements();
  filter(BLUR, 10);
  drawCross();
}

function drawElements() {
  let num = 15;
  let angleStep = 360 / num;

  push();
  translate(width / 2, height / 2);
  rotate(radians(frameCount % 360)); // Rotate based on frameCount for animation
  
  noStroke();
  fill('red');
  
  for (let i = 0; i < num; i++) {
    ellipse(50, 60, 700, 10);
    rotate(angleStep);
  }
  
  pop();
}

function drawCross() {
  strokeWeight(1);
  stroke('white');
  fill('white');
  rect(windowWidth / 2 - 5, windowHeight / 2 - 60, 10, 150);
  rect(windowWidth / 2 - 38, windowHeight / 2 + 40, 75, 10);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}