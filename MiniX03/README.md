# MiniX03: Demon Throbber

![p5.js throbber with a cross in the middle](https://scontent-cph2-1.xx.fbcdn.net/v/t1.15752-9/432951118_1328644954476268_3977717116974792627_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=5f2048&_nc_ohc=IrSa7MHVUcwAX_r6DkH&_nc_ht=scontent-cph2-1.xx&oh=03_AdRR3SAi4AK_9T_lfYXMM1N4i-5U7PW1W1ua2ObhWXAh9g&oe=661B889C)
<br>

Please run the code [here](https://editor.p5js.org/willemussemand/full/GiTc04lg2)
<br>
Please view the full repository [here](https://gitlab.com/WilhelmYAYA/aesthetic-programming/-/tree/main/miniX03)
<br>

### THE PROJECT

For miniX 3 we were tasked with creating a throbber, with a deeper meaning. 

When I encounter throbbers in my daily life, it is usually an annoyance. The thobber shows the user that the computer is trying to load something, and this can sometimes be a little scary...

maybe my computer fails?
maybe i cant see the new episode of my TV show?

I'm not actually serious when I say that throbbers intill fear in my heart, but I thought it was an interesting concept. In order to have a little fun with the concept of time in computing, and the fear that it may instill in the user, I made a satanic, extremely slow, glitchy, throbber.

Whats scarier than the devil? The devil as a throbber...

### HOW I MADE IT 

I centered everything using `windowWidth / 2, windowHeight /2`.

I also made the window size changable using this function, `function windowResized() {resizeCanvas(windowWidth, windowHeight);`.

##### I started by making a basic throbber:

This involved creating a function that created a circle. I then divided the angle of the elements i wanted in my throbber's movement, by `num`, 15 `et cir = 360 / num` with `num` being, 15, the number of elements in the throbber. This divided the elements equally on the circle, and told them how to move. I used `(frameCount % num)` in order to give them a pace to move at.

The elements I used for my thobber started out as basic ellipses, but i decided to strech them out alot, to make them look spiky and a little evil.

##### Triangle and Cross:

I added a basic yellow upside down triangle to the piece, simply because it looked kind of demonic. and the same goes for the upside down cross...

##### Filter:

I also put a blur filter on the piece with, `filter(BLUR, 10)`. I use a web editor for writing in p5.js and it cant handle this effect, which glitches it out completely, making the piece very laggy. I really think this works in its favor as it gives it a slow, and broken look.




