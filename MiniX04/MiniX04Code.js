let capture;
  

function setup() {
  createCanvas(640, 480);
  
  // creation of the video capture object
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide(); // hide the video element, as we will be adding it in manually later

  
  // creation of the slider
  alphaSlider = createSlider(50, 256, 50);
  alphaSlider.position(width/2-70, 460)
   
  //creation of a checkbox
  checkbox = createCheckbox('terms and conditions')
  checkbox.position(0, 455);
}

function draw() {
  background(220);
  

  // draw the webcam feed onto the canvas

  image(capture, 0, 0, width/2, height/2); //top left
  //filter(GRAY);
  image(capture, width/2, 0, width/2, height/2); //top right
  //filter(INVERT);
  image(capture, 0, height/2, width/2, height/2); //bottom left
  //filter(THRESHOLD);
  image(capture, width/2, height/2, width/2, height/2); //bottom right
  //filter(OPAQUE);
  
  //I really wanted to add different filters on the different video capupture elements i drew, but the web editor for p5.js gets to glitchy.
  
  let r = color(255, 0, 0); //red
  let g = color(0, 255, 0); //green
  let b = color(0, 0, 255); //blue
  
  
  //LET THE SLIDER CONTROL THE ALHPA VALUE OF OVERLAYS
  
  let alphaValue = alphaSlider.value();
  
  noStroke();
  r.setAlpha(alphaValue);
  g.setAlpha(alphaValue);
  b.setAlpha(alphaValue);
  
  
  //3 RGB OVERLAYS ON SCREEN
  
  //Top right screen
  fill(r);
  rect(width/2, 0, width/2, height/2);

  //bottom left screen
  fill(g);
  rect(0, height/2, width/2, height/2);
  
  //bottom right screen
  fill(b);
  rect(width/2, height/2, width/2, height/2);
  
  
  //GLITCHY EFFECT
  
  // check if the mouse is in the top-left quadrant, and if the terms and conditions are accepted
  if (mouseX < width / 2 && mouseY < height / 2 && checkbox.checked()) {
    isGlitching = true; 
  } else {
    isGlitching = false;
  }
  
  // apply the appropriate background effect, including glitch effect and "recording, reporting" message
  
  if (isGlitching) {
    if (random(1) < 0.5) {
      background(0)
  }
    //parameters for the text
    textSize(50);
  textAlign(CENTER, CENTER);
    
    
 fill(255);
  text("RECORDING, REPORTING", width/2, height/2-40);
    
 fill(255,0,0);
  text("RECORDING, REPORTING", width/2, height/2+40);
} 

  
  //If the checkbox is checked, the free trail texrt goes away
  
  if (!checkbox.checked()) {
     textSize(100);
    fill(0);
    textAlign(CENTER, CENTER);
  
    text("FREE TRAIL", width/2, height/2);
    
  }
  fill(200);
  rect(0,height-30,width,30, 50, 50, 0, 0);
  
}