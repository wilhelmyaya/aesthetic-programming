# MiniX04: Payment by data

![Fun filter software wuhuuu!](https://scontent.faar2-2.fna.fbcdn.net/v/t1.15752-9/430045443_336108542782842_4683948631776639286_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=5f2048&_nc_ohc=mcIgHTgkGp4AX_cEKj_&_nc_ht=scontent.faar2-2.fna&oh=03_AdRGcfoRF9r6rIb_1o4NNPwaHfXbhNNYnGjWvISC3ZQ6qw&oe=662E249B)
<br>
Please run the code [here](https://editor.p5js.org/willemussemand/full/edbIKxlRV)
<br>
Please view the full repository [here](https://gitlab.com/WilhelmYAYA/aesthetic-programming/-/tree/main/miniX03)
<br>

### THE PROJECT
For MiniX04 we were tasked with creating a piece of software, that utilizes different forms of user inputs. These could include, the usage of a microphone, camera recording, mouse tracking, keyboard key strokes, etc, etc. We were to create something that in some way sparked a political conversation about surveilance, and data gathering. 

I decided to create a program that looks alot like camera filter software, such as photobooth. Basically its an interactive piece of software, that allows you to play around with your webcam, adding, and changing 3 different 'filters' (they arent really filters, they are actually just color overlays). 

There is a big piece of text that says, "FREE TRAIL" on the screen, and that doesnt go away unless you accept the terms and conditions of the software. When you do this, (obviously without reading the terms and conditions), you unwittingly are selling your information, thereby, sort of paying for the software. And thus, the free trail text goes away. 

In many other pieces of software, such as facebook, or really any other social media platform, you arent made aware that you are actively being, watched, listened to, and reported on. I wanted to make this visible, so i made it so that if you hover your mouse on a certain part of the screen, it starts glitching out, and the text, "RECORDING, REPORTING" pops up. In this way, I am unveiling the truth of my program. 

### HOW I MADE IT
The whole project was made in an iterative way. There really wasnt a plan when I began making the software, I just came up with new ideas on the spot in order to get my end product. 

##### THE CAMERA 
I started by creating a video capture object, and then hiding it, as I would be adding it later. I then called the camera footage in my `function draw()`, four times, being sure to place each of them in their own quadrant of the screen. I did this using this code,  

 `image(capture, 0, 0, width/2, height/2);`
 <br>
 `image(capture, width/2, 0, width/2, height/2);`
 <br>
 `image(capture, 0, height/2, width/2, height/2);` 
 <br>
 `image(capture, width/2, height/2, width/2, height/2);`

I really wanted to add different effects on the different camera elements, but p5.js' web editor couldnt handle it, without lagging and crashing, so i insted opted to putting in colored overlays, with low opacity. 

In order to create the different overlays, I created 3 rectangles with the same dimensions as 3 of the camera elements, defining the color of each of the boxes, seperately from the creation of the boxes;

I started by defining my 3 colors, red, green, and blue:
`let r = color(255, 0, 0);`
<br>
`let g = color(0, 255, 0);`
  <br>
`let b = color(0, 0, 255);`

Then I created the rectangles, using`fill()` to fill them with my predefined colors:
 `fill(r);`
<br>
 `rect(width/2, 0, width/2, height/2);`
<br>
  `fill(g);`
<br>
  `rect(0, height/2, width/2, height/2);`
  <br>
  `fill(b);`
  <br>
  `rect(width/2, height/2, width/2, height/2);`

The low opacity of the RGB rectangles was achieved by creating a slider that would change the `setAlpha()` value, of the different overlays.

##### THE SLIDER 
I started by creating a slider, and calling it alphaSlider; `alphaSlider = createSlider(50, 256, 50);` I then created a variable which I called alphaValue, and connected it to the value of by alphaSlider, using:
`let alphaValue = alphaSlider.value();`

Then, in order to allow variable alphaValue, and thereby the alphaSlider, to control the setAlpha value of the rectangles I wrote this code:
`r.setAlpha(alphaValue);`
<br>
`g.setAlpha(alphaValue);`
<br>
`b.setAlpha(alphaValue);`

All this code allows me to control the opacity of 3 different RGB colored overlays, creating the effect of a "filter."

##### GLITCHY EFFECT 
I started by creating a "glitchy" effect. I did this by creating a function that randomly turns the screen black at a fast pace, calling the function, isGlitching;

`if (isGlitching) {if (random(1) < 0.5) {background(0)}`

this code rapidly generates a number between 0 and 1, and if the number is lesser than 0.5, it makes the background black.
In addition to this, the if statement also contains:

` textSize(50);`
<br>
  `textAlign(CENTER, CENTER);`
<br>

 `fill(255);`
<br>
  `text("RECORDING, REPORTING", width/2, height/2-40);`
<br>

`fill(255,0,0);`
<br>
`text("RECORDING, REPORTING", width/2, height/2+40);`
`}`

This if statement makes it so that if isGlitchy is true, the screen "glitches" and the text, "RECORDING, REPORTING" x2 pops up on the screen.

I wanted to add a fake, terms and conditions checkbox, as part of the parameters for when isGlitching is active, so I started by making a simple checkbox, simply calling it checkbox.

`checkbox = createCheckbox('terms and conditions')`

I wanted to add mouse input into the program, so I created an event listener in the form of an if statement, which would activate the isGlitchy function.

`if (mouseX < width / 2 && mouseY < height / 2 && checkbox.checked()) `
<br>
`isGlitching = true;` 
<br>
`} else {`
<br>
`isGlitching = false;`
<br>
`}`

This piece of code, `if (mouseX < width / 2 && mouseY < height / 2` is an event listener that checks of the mouse is in the top left quadrant of the canvas, and this, `checkbox.checked())` piece, checks of the checkbox is checked.

if both these are true, isGlitching is true, making the screen "glitch", and making the aformentioned text appear.

The final piece of code in my program, makes it so that the "FREE TRIAL" text appears, whenever the terms and conditions the checkbox isn't checked. I did this by writing yet another if statement:

`if (!checkbox.checked()) {`
<br>
`textSize(100);`
<br>
`fill(0);`
<br>
`textAlign(CENTER, CENTER);`
<br>
`text("FREE TRIAL", width/2, height/2);`    
<br>
`}`

This if statement is saying that, if checkbox.checked() is false, then a piece of text saying "FREE TRIAL" will appear on the screen.




