function setup() {
  createCanvas(windowWidth, windowHeight); // Create a canvas that fits the entire window
}

function draw() {
  background(0, 20); // Set the background to black with 20% opacity for a trailing effect
  stroke(255, 255, 255); // Default stroke color for the ellipses to white
  
  let noiseLevel = windowHeight; // Maximum height the wave can reach
  let noiseScale = 0.01; // Controls the frequency of the noise
  
  // Iterate from left to right across the width of the canvas
  for (let x = 0; x < windowWidth; x += 1) {
    let nx = noiseScale * x; // Scale the x value for the noise function
    let nt = noiseScale * frameCount; // Add a time component to the noise function
    
    // Calculate the y position based on Perlin noise
    let y = noiseLevel * noise(nx, nt);
    
    // Conditional statement to change color based on y position
    if (y > windowHeight / 2) {
      stroke('red'); // Change stroke color to red if y is greater than half the window height
    } else {
      stroke('white'); // Otherwise, keep it white
    }
    
    noFill(); // Ensure ellipses are not filled
    ellipse(x, windowHeight / 2, x, y); // Draw ellipses at the calculated positions
  }
}