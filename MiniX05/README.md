# MiniX05: Randomly generated elliptical wave 

![crazy elliptical wave](https://scontent-cph2-1.xx.fbcdn.net/v/t1.15752-9/435559585_1174770950352784_351590938156667576_n.jpg?stp=dst-jpg_s2048x2048&_nc_cat=106&ccb=1-7&_nc_sid=5f2048&_nc_ohc=mUCL9FyQDdwAb71jjew&_nc_ht=scontent-cph2-1.xx&oh=03_AdX5zQY-1LLgY0S0MsejbjVVjjhR-KhxfF_J2YO6Za-C8Q&oe=663BADEC)
<br>
Please run the code [here](https://editor.p5js.org/willemussemand/full/VosxIXHGU)
<br>
Please view my full repository [here](https://gitlab.com/WilhelmYAYA/aesthetic-programming/-/tree/main/miniX05)
<br>

### THE PROJECT

I have created a randomly generating, and constantly changing wave, made of ellipses. I got the idea when researching p5's random generation capabilities. I looked at p5's refrence sheets, but found that most of the things in p5 that allow for random generation, such as random(), or randomGaussian() can return results that vary alot from the original input, and in this way, it is truly and completely unpredictable. 

For my project however, I wanted something that was a little more "organic" feeling. Something that constantly iterated on itself. Something that almost looks alive, and gives the illusion of particles building upon eachother. I therefore chose to use noise() for this project. noise level allowed me to give it an input, and control the spread of the answers it gave in return.

I based my work on the work found under noise() in the p5 refrences.

### THE PROCESS

I ended up creating a line in the middle of the screen, going from left to right, where several ellipses were placed, with theur y value changing and iterating on itself by a factor of 1, giving it a crazy wave effect. I also added a level of transparency to the background, giving the whole thing a sort of blurry, or organic feeling movement.

##### REFRENCES 

[noise() p5 refrence page](https://p5js.org/reference/#/p5/noise)

