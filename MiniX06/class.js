class Player {
  constructor(x, y, key1, key2) {
    this.rectXLoad = x;
    this.rectYLoad = y;
    this.rectX = x;
    this.rectY = y;
    this.rectWidth = 50;
    this.rectHeight = 10;
    //where the rect can go
    this.rectMin = 0; 
    this.rectMax = 500 - this.rectWidth;
    this.rectSpeed = 10;
    this.keycode = key1; 
    this.keyCode2 = key2;
  }

  display() {
    noFill();
    stroke(255, 0, 0);
    rect(this.rectX, this.rectY, this.rectWidth, this.rectHeight);
  }

  move() {
    if (keyIsDown(this.keycode)) {
      this.rectX -= this.rectSpeed;
    } else if (keyIsDown(this.keyCode2)) {
      this.rectX += this.rectSpeed;
    }
  }

 boundsDetection() {
    if (this.rectX < this.rectMin) {
      this.rectX = this.rectMin;
   } else if (this.rectX > this.rectMax) {
    this.rectX = this.rectMax;
  }
}
  
  reload() {
    this.rectX = this.rectXLoad;
    this.rectY = this.rectYLoad;
  }
}

class Ball {
  constructor() {
    this.ballSize = 40;
    this.ballX = 500 / 2 - this.ballSize / 2;
    this.ballY = 0 + this.ballSize / 2;
    this.ballSpeedX = 2;
    this.ballSpeedY = 7;
  }

  display() {
    noFill();
    stroke(255, 0, 0);
    strokeWeight(3);
    ellipse(this.ballX, this.ballY, this.ballSize);
  }

  randomSpeed() {
    this.ballSpeedY = -5;
  }

  move() {
    this.ballX += this.ballSpeedX;
    this.ballY += this.ballSpeedY;
  }
  
  boundsDetection() {
    if (this.ballX < 0 + this.ballSize / 2 || this.ballX > 500 - this.ballSize / 2) {
        this.ballSpeedX *= -1;
    }
    if (this.ballY < 0 + this.ballSize / 2 || this.ballY > 500 - this.ballSize / 2) {
        this.ballSpeedY *= -1;
        if (this.ballY > 500 - this.ballSize / 2) {
            this.reload();
            player.reload();
        }
    }
}

  reload() {
    this.ballX = 250 + this.ballSize / 2;
    this.ballY = 0 + this.ballSize;
    this.ballSpeedX = 5;
    this.ballSpeedY = 5;
  }
}