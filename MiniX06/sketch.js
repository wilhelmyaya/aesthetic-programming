let player;
let ball;
let scoreValue = 0;

let playerMarginX = 225;
let playerMarginY = 450;

function setup() {
  createCanvas(500, 500);
  player = new Player(playerMarginX, playerMarginY, 37, 39);
  ball = new Ball();
}

function draw() {
  background(40);
  frameRate(90);
  
  //rectangle
  player.display();
  player.move();
  player.boundsDetection();

  //ball
  ball.display();
  ball.move();
  ball.boundsDetection();

  //Collision detection and score reset
  collisionDetection();
  textSize(30);
  text(scoreValue, 250, 40);
}

// Function to handle collision detection and score reset
function collisionDetection() {
  //check if ball touches rectangle
  if (
    ball.ballY + ball.ballSize / 2 >= player.rectY &&
    ball.ballY <= player.rectY + player.rectHeight &&
    ball.ballX + ball.ballSize >= player.rectX &&
    ball.ballX <= player.rectX + player.rectWidth
  ) {
    // Handle collision
    ball.ballSpeedY *= -1.3; // Reverse vertical speed
    scoreValue++; //adds one to the score value
  } else if (ball.ballY + ball.ballSize / 2 >= height) {
    // Reset score if the ball hits the bottom of the canvas
    resetScore();
  }
}

// Function to reset the score
function resetScore() {
  console.log("Score reset condition met");
  scoreValue = 0;
}
